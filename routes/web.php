<?php

use App\Postcard;
use App\PostcardSendingService;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// normal view
Route::get('charge', 'PayOrderController@store');
Route::get('channels', 'ChannelController@index');
Route::get('posts', 'PostController@index')->name('post');
Route::get('posts/create', 'PostController@create')->name('post.create');

// create Post + tags
Route::post('posts/createPost', 'PostController@createPost');

// Facades
Route::get('/postcards', function () {
    $postcardService = new PostcardSendingService('us', 4, 6);
    $postcardService->hello("Hello My Fans!!!", 'mc@mail.com');
});
Route::get('/postcards2', function () {
    // Postcard.php will be creating dynamic function calling like below
    // app()['Postcard']->hello("Hello My Fans Using Facades!!!", 'mc@mail.com');
    Postcard::hello("Hello My Fans Using Facades!!!", 'mc@mail.com');
});

// Macros
Route::get('/partNumber', function(){
    dd(Str::partNumber('ABC123456789'));
});
Route::get('/prefix', function(){
    dd(Str::prefix('myprefix', 'table-'));
});
Route::get('/errorJson', function(){
    dd(ResponseFactory::errorJson(404, "NOT FOUND !!!   "));
});


// Repository
Route::get('/customers', 'CustomerController@all');
Route::get('/customers/{id}', 'CustomerController@getById');


