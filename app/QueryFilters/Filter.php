<?php

namespace App\QueryFilters;
use Illuminate\Support\Str;

abstract class Filter
{
    protected function filterName()
    {
        return Str::snake(class_basename($this));
    }

    protected function getRequestParameter()
    {
        return request()->get($this->filterName());
    }
}