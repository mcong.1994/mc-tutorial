<?php

namespace App\QueryFilters;

use Closure;

class Active extends Filter
{
    public function handle($request, Closure $next)
    {
        $request_parameter = $this->getRequestParameter();
        if ($request_parameter != null && ($request_parameter == 1 || $request_parameter == 0)) {
            $builder = $next($request);
            return $builder->where('active', $request_parameter);
        }
        return $next($request);
    }
}
