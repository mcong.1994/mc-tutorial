<?php

namespace App\QueryFilters;

use Closure;

class MaxCount extends Filter
{
    public function handle($request, Closure $next)
    {
        $request_parameter = $this->getRequestParameter();
        if (!empty($request_parameter)) {
            $builder = $next($request);
            return $builder->take($request_parameter);
        }
        return $next($request);
    }
}
