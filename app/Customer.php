<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    // Format set and use in repository
    public function format()
    {
        return [
            'customer_id' => $this->id,
            'user_id' => $this->user->id,
            'customer_name' => $this->name,
            'email' => $this->user->email,
        ];
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
