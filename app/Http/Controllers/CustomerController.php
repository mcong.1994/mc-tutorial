<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Repositories\CustomerRepositoryInterface;
use Illuminate\Http\Request;
use App\Repository\CustomerRepository;

class CustomerController extends Controller
{
    private $customerRepository;

    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function all()
    {
        $customers = $this->customerRepository->all();
        // $customers = Customer::orderBy('name')
        //     ->where('active', 1)
        //     ->with('user')
        //     ->get()
        //     ->map(function ($customer) {
        //         return [
        //             'customer_id' => $customer->id,
        //             'user_id' => $customer->user->id,
        //             'customer_name' => $customer->name,
        //             'email' => $customer->user->email,
        //         ];
        //     });
        return view('customer.index', compact('customers'));
    }

    public function getById()
    {
        $customer_id = request('id');
        $customer = $this->customerRepository->findById($customer_id);
        
        return $customer;
        return view('customer.view', compact('customer'));
    }
}
