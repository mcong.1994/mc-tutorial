<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{

    public function index()
    {
        // get base class name --> PostController
        // dd(class_basename($this));

        // 1st way of query with dynamic condition
        // $posts = Post::query();
        
        // if(request()->active == 1 || request()->active == 0){
        //     $posts->where('active', request()->active);
        // }
        // if(!empty(request()->sort)){
        //     $posts->orderBy('title', request()->sort);
        // }
        // $posts = $posts->get();

        // Pipeline way of query with dynamic condition
        $pipeline = app(Pipeline::class)
            ->send(Post::query())
            ->through([
                \App\QueryFilters\Active::class,
                \App\QueryFilters\Sort::class,
                \App\QueryFilters\MaxCount::class,
            ])
            ->thenReturn();
        $posts = $pipeline->get();

        return view('post.index', compact('posts'));
    }

    public function create()
    {
        return view ('post.create', compact('channels'));
    }

    public function createPost(Request $request)
    {
        $post = Post::create([
            'title' => $request->title,
            'body' => $request->body
        ]);
        if(!empty($request->tag)){
            $post->tags()->create([
                'name' => $request->tag
            ]);
        };
        if($post){
            Session::flash('success', 'Insert Success');
            return redirect()->route('post.create');
        };
       
    }
}
