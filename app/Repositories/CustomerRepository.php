<?php 

namespace App\Repository;

use App\Customer;
use App\Repositories\CustomerRepositoryInterface;

class CustomerRepository implements CustomerRepositoryInterface
{
    // ->format() IS DEFINED IN CUSTOMER MODEL
    public function all()
    {
        return Customer::orderBy('name')
            ->where('active', 1)
            ->with('user')
            ->get()
            ->map->format();
    }

    public function findById($customerId)
    {
        $customer = Customer::where('id', $customerId)
            ->where('active', 1)
            ->with('user')
            ->firstOrFail()
            ->format();
        return $customer;
    }

    public function delete(){}
    public function update(){}
}