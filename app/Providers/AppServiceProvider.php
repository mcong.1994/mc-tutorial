<?php

namespace App\Providers;

use App\Billing\BankPaymentGateway;
use App\Billing\CreditPaymentGateway;
use App\Billing\PaymentGatewayContract;
use App\Channel;
use App\Http\View\Composers\ChannelsComposer;
use App\Mixins\StrMixins;
use App\PostcardSendingService;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PaymentGatewayContract::class, function () {
            if (request()->get('credits') === 'true') {
                return new CreditPaymentGateway('usd');
            }
            return new BankPaymentGateway('usd');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // COMPOSER START
        // ALL VIEW WILL BE USING
        // View::share('channels', Channel::orderBy('name')->get());

        // specific view or wildcard view
        // View::composer(['channel.index', 'post.*'], function($view){
        //     $view->with('channels', Channel::orderBy('name')->get());
        // });

        // Dedicated class
        View::composer(['partials.channels.*'], ChannelsComposer::class);

        // COMPOSER END


        // FACADES START
        $this->app->singleton('Postcard', function ($app) {
            return new PostcardSendingService('us', 4, 6);
        });

        // FACADES END

        // Macro
        // Str::macro('partNumber', function($part){
        //     return 'AB-'.substr($part, 0, 3).'-'.substr($part, 3);
        // });
        Str::mixin(new StrMixins());

        // ResponseFactory::macro('errorJson', function ($status_error, $message = 'Default Error Message'){
        //     return [
        //         'status' => $status_error,
        //         'message' => $message,
        //     ];
        // });      
    }
}
