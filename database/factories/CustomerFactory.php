<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use Carbon\Carbon;
use Faker\Generator as Faker;

$autoIncrement = autoIncrement();
$factory->define(Customer::class, function (Faker $faker) use ($autoIncrement) {
    $autoIncrement->next();
    return [
        'user_id' => $faker->unique()->numberBetween(1, App\User::count()),
        'name' => $faker->name,
        'contacted_at' => Carbon::now()->format('Y-m-d H:i:s'),
        'active' => rand(0, 1),
    ];
});


function autoIncrement()
{
    for ($i = 1; $i < 100; $i++) {
        yield $i;
    }
}
