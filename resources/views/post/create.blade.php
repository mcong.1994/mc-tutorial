<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Create Post</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <form action="/posts/createPost" method="post">
            @csrf
            @include('partials.channels.dropdown', ['field'=>'my_channel_id'])
            <div style="margin-top:20px">
                <div> title and body will store in db</div>
                <div><input type="text" name="title" value="" placeholder="Title"></div>
                <div><textarea name="body" id=""  rows="5" placeholder="Body"></textarea></div>
                <div><input type="text" name="tag" value="" placeholder="Tag"></div>
                <div><button type="submit">Add</button></div>
                @if (Session::has('success'))
                    <span style="color:green">{!! session('success') !!}</span>
                @endif
            </div>
            
            
        </form>
    </body>
</html>
