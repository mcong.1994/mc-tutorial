<select name="{{$field ?? 'channel_id'}}" id="{{$field ?? 'channel_id'}}">
    @foreach ($channels as $row)
        <option value="{{$row->id}}">{{$row->name}}</option>
    @endforeach
</select>